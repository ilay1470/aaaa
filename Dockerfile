FROM node:15

ENV DEBIAN_FRONTEND=noninteractive

ARG USERNAME=node
ARG USER_UID=1000
ARG USER_GID=$USER_UID


RUN "apt-get update" 
RUN "apt-get install -y --no-install-recommends \
        apt-utils \
        bash-completion \
        ca-certificates \
        curl \
        dialog \
        git \
        iproute2 \
        jq \
        procps \
        less \
        vim"
    #
    # Install bash completion
 RUN "echo ". /etc/bash_completion" >> ~/.bashrc \
    && mkdir -p ~/completions \
    #
    # Install node packages globally
    && npm install -g eslint pino-pretty \
    #
    # Add add sudo support for non-root user
    && apt-get install -y sudo \
    && echo node ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME"
